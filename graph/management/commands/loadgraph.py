from django.core.management import BaseCommand
from graph import models
import csv
import sys


class Command(BaseCommand):
    help = u'Load graph data into database.'


    def handle(self, *args, **options):
        n = 0
	with sys.stdin as csvfile:
            reader = csv.reader(csvfile, quotechar='"')
	    for row in reader:
	        assert len(row) >= 2
	        source, target = row[0], row[1]
                category = row[2] if len(row) > 2 else 'default'
                data = row[3] if len(row) > 3 else None
                e = models.Edge(source=source, target=target, category=category, data=data)
                e.save()
                models.Node.objects.get_or_create(name=source)
                models.Node.objects.get_or_create(name=target)
                n += 1
        print "Loaded %d edges." % n
